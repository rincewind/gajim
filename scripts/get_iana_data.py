#!/usr/bin/env python3

import argparse
import csv
import io
import logging
import re
import sys
from datetime import datetime
from pathlib import Path
from urllib.request import urlopen

SCHEMES_URL = 'https://www.iana.org/assignments/uri-schemes/uri-schemes-1.csv'

logging.basicConfig(level='INFO', format='%(levelname)s: %(message)s')


def download_file(url: str) -> str:
    logging.info('Download: %s', url)
    with urlopen(url) as f:
        content = f.read()
    return content.decode()


def parse_uri_schemes(content: str) -> list[str]:
    logging.info('Parse uri schemes')
    schemes: list[str] = []

    reader = csv.reader(io.StringIO(content), delimiter=',')
    next(reader)  # Skip header row
    for line in reader:
        scheme = line[0].lower().removesuffix(' (obsolete)')
        if not re.fullmatch('[a-z0-9+.-]+', scheme):
            logging.warning('unexpected scheme field contents: %s', scheme)
            continue

        schemes.append(scheme)

    return schemes


def generate_output(schemes: list[str],
                    outpath: Path) -> None:

    logging.info('Generate output')
    current_date = datetime.utcnow().isoformat()
    content = f'# Generated by get_iana_data.py @ {current_date}\n'

    content += '\n'

    content += 'URI_SCHEMES = {\n'
    for scheme in schemes:
        content += f"    '{scheme}',\n"
    content += '}\n'

    outpath.write_text(content)
    logging.info('Wrote file to %s', outpath)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate IANA data')
    parser.add_argument('out', help='Path to output file')
    args = parser.parse_args()

    outpath = Path(args.out)
    if outpath.is_dir():
        sys.exit('Output path is a directory')

    scheme_content = download_file(SCHEMES_URL)
    schemes = parse_uri_schemes(scheme_content)
    generate_output(schemes, outpath)
    logging.info('Finished !')
